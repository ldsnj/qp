﻿using UnityEngine;
using System.Collections;

public class PlySoundOnHit : MonoBehaviour {

	public AudioClip hitSound;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

	}

		void OnCollisionEnter2D(Collision2D other)
		{
			foreach(ContactPoint2D contact in other.contacts)
			{
			 audio.PlayOneShot(hitSound);
			}
		}


}
