﻿using UnityEngine;
using System.Collections;

public class LeakFix : MonoBehaviour {


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		if(other.collider2D.gameObject.name.Equals("Bucket"))
		{
			// WIN THE GAME
			if(Mathf.Abs (other.collider2D.gameObject.rigidbody2D.angularVelocity) < 10.0f) 
			{
				GameObject go = GameObject.Find("GameManager").gameObject;
				GameManager gm = go.GetComponent<GameManager>();
				gm.startMiniGame ();
			}

		}

	}

	void OnTrigger2D(Collider2D other)
	{
		if(Mathf.Abs(other.collider2D.gameObject.rigidbody2D.angularVelocity) < 10.0f) 
		{
			GameObject go = GameObject.Find("GameManager").gameObject;
			GameManager gm = go.GetComponent<GameManager>();
			gm.startMiniGame ();
		}

	}
}
