﻿using UnityEngine;
using System.Collections;

public class SparkOnCollision : MonoBehaviour {

	public GameObject spark;

	void OnCollisionEnter2D(Collision2D other)
	{
		foreach(ContactPoint2D contact in other.contacts)
		{
			Instantiate(spark,contact.point,Quaternion.identity);
		}
	}


}
