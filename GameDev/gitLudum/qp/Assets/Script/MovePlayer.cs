﻿using UnityEngine;
using System.Collections;

public class MovePlayer : MonoBehaviour {


	public float speed = 0.04f;

	private GameObject go;
	private GameObject handler;

	// Use this for initialization
	void Start () {
		go = GameObject.Find ("Bucket").gameObject;
		handler = GameObject.Find ("Corde").gameObject;
	}
	
	// Update is called once per frame
	void Update () {
	
		if(Input.GetKeyDown(KeyCode.DownArrow) || Input.GetKey ("down"))
		{
			transform.position += new Vector3(0, -speed, 0);
			go.rigidbody2D.angularDrag = 0.05f;

		}
		if(Input.GetKeyDown (KeyCode.UpArrow) || Input.GetKey ("up"))
		{
			if(handler.gameObject.transform.position.y < 2)
			{
				transform.position += new Vector3(0, +speed, 0);
				go.rigidbody2D.angularDrag = 0.05f;
			}
		}
		if(Input.GetKeyDown (KeyCode.LeftArrow) || Input.GetKey ("left"))
		{
			transform.position += new Vector3(-speed, 0, 0);
			go.rigidbody2D.angularDrag = 0.05f;
		}
		if(Input.GetKeyDown (KeyCode.RightArrow) || Input.GetKey ("right"))
		{
			transform.position += new Vector3(+speed, 0, 0);
			go.rigidbody2D.angularDrag = 0.05f;
		}

		if(Input.GetKey("down")  == false &&
		   Input.GetKey("up")    == false &&
		   Input.GetKey("left")  == false &&
		   Input.GetKey("right") == false)
		{
			go.rigidbody2D.angularDrag = 1.0f;
		}
	}
}
