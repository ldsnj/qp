﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour {


	public GameObject miniGame;
	public GameObject leakObject;
	public GameObject timerLabel;
	public GameObject healthLabel;
	public GameObject levelLabel;
	public GameObject uiWin;
	public GameObject uiLost;
	public GameObject uiMainMenu;
	public GameObject uiCreditMenu;
	public GameObject uiInstructions;
	public GameObject uiPanelInfo;
	public GameObject uiPanelMiniGame;
	public GameObject playerRef;
	public GameObject uiPanelPause;

	public GameObject menuAudio;

	public float maxTime = 0.0f;
	private float timeElapsed = 0.0f;
	private int displayTime = 0;
	private float timeLeft = 0.0f;
	private bool lblFPSActive = false;

	public GameObject bucket,rope;
	private Vector3 bucketInitialPos, ropeInitialPos;




	enum GameStates {Pause, Run, Menu, Win, MiniGame, GameOver, Credits, Instructions};

	private GameStates gameStates = GameStates.Menu;
	// Use this for initialization
	void Start () {
		bucketInitialPos = bucket.transform.position;
		ropeInitialPos = rope.transform.position;
		gameStates = GameStates.Menu;
		miniGame.gameObject.SetActive (false);
		timeElapsed = Time.time;
		timeLeft = maxTime - timeElapsed;

		displayTime = (int)Mathf.RoundToInt(timeLeft);
		timerLabel.gameObject.GetComponent<UILabel>().text = "Timer Left : "+ displayTime;
	
		// On cache les label de health et de timer
		timerLabel.gameObject.SetActive(false);
		healthLabel.gameObject.SetActive(false);
		levelLabel.gameObject.SetActive(false);
		Time.timeScale = 0;

	}
	
	// Update is called once per frame
	void Update () {
	
		timeElapsed += Time.deltaTime;

		// Update the display of the timer
		timeLeft = maxTime - timeElapsed;
		displayTime = (int)Mathf.RoundToInt (timeLeft);
		timerLabel.gameObject.GetComponent<UILabel>().text = "Timer Left : "+ displayTime;

		if(timeElapsed >= maxTime && gameStates != GameStates.Win)
		{
			gameStates = GameStates.GameOver;
     	}

		if(gameStates == GameStates.MiniGame)
		{
			miniGame.gameObject.SetActive (true);
			uiPanelMiniGame.gameObject.SetActive (true);
		}
		else if(gameStates == GameStates.Win)
		{
			miniGame.gameObject.SetActive (false);
			Destroy (leakObject);
			uiPanelMiniGame.gameObject.SetActive(false);
			uiWin.gameObject.SetActive(true);
			timeElapsed = 0.0f;
			Time.timeScale = 0;


		}
		else if(gameStates == GameStates.GameOver)
		{
			miniGame.gameObject.SetActive (false);
			uiLost.gameObject.SetActive(true);
			uiPanelMiniGame.gameObject.SetActive(false);
			timeElapsed = 0.0f;
			Time.timeScale = 0;

		}else if(gameStates == GameStates.Menu)
		{
			// In Main menu
			uiMainMenu.gameObject.SetActive (true);
			Camera.main.audio.Stop ();
			if(!menuAudio.audio.isPlaying)
				menuAudio.audio.Play ();

			uiInstructions.gameObject.SetActive (false);
			uiCreditMenu.gameObject.SetActive (false);
			uiWin.gameObject.SetActive(false);
			uiLost.gameObject.SetActive(false);
			playerRef.gameObject.GetComponent<PlayerHealth>().resetHealth();
			Time.timeScale = 0;
		}
		else if(gameStates == GameStates.Run)
		{
			// Start the game! 
			// Hide main menu
			uiMainMenu.gameObject.SetActive (false);
			menuAudio.audio.Stop ();
			// We start the timer!
			Time.timeScale = 1;

			// Display the label GUI!
			timerLabel.gameObject.SetActive(true);
			healthLabel.gameObject.SetActive(true);
			levelLabel.gameObject.SetActive(true);
			uiPanelPause.gameObject.SetActive (false);


		}
		else if(gameStates == GameStates.Credits)
		{
			uiMainMenu.gameObject.SetActive (false);
			Camera.main.audio.Stop ();
			uiCreditMenu.gameObject.SetActive (true);
			Time.timeScale = 0;
			timerLabel.gameObject.SetActive (false);
			healthLabel.gameObject.SetActive(false);
			levelLabel.gameObject.SetActive(false);

		}
		else if(gameStates == GameStates.Instructions)
		{
			uiMainMenu.gameObject.SetActive (false);
			Camera.main.audio.Stop ();
			uiCreditMenu.gameObject.SetActive(false);
			Time.timeScale = 0;
			timerLabel.gameObject.SetActive (false);
			healthLabel.gameObject.SetActive (false);
			levelLabel.gameObject.SetActive(false);

			uiInstructions.gameObject.SetActive(true);


		}
		else if(gameStates == GameStates.Pause)
		{
			uiPanelPause.gameObject.SetActive (true);
			Time.timeScale = 0;

		}

		if(Input.GetKeyDown(KeyCode.F3))
		{

			if(lblFPSActive == false)
			{
				uiPanelInfo.gameObject.SetActive(true);
				lblFPSActive = true;
			}
			else
			{
				uiPanelInfo.gameObject.SetActive (false);
				lblFPSActive = false;
			}
			
		}

		if(Input.GetKeyDown (KeyCode.Escape))
		{
			if(GameStates.Run == gameStates)
			{
				gameStates = GameStates.Pause;
			}
			else if(GameStates.Pause == gameStates)
			{
				gameStates = GameStates.Run;
			}

		}
	}

	public void startGame()
	{
		bucket.transform.position = bucketInitialPos;
		rope.transform.position = ropeInitialPos;

		timeElapsed = 0.0f;
		displayTime = 0;
		timeLeft = 0.0f;
		lblFPSActive = false;

		miniGame.gameObject.SetActive (false);
		timeElapsed = 0.0f;
		timeLeft = maxTime - timeElapsed;
		
		displayTime = (int)Mathf.RoundToInt(timeLeft);
		timerLabel.gameObject.GetComponent<UILabel>().text = "Timer Left : "+ displayTime;
		
		// On cache les label de health et de timer
		timerLabel.gameObject.SetActive(false);
		healthLabel.gameObject.SetActive(false);
		levelLabel.gameObject.SetActive(false);


		menuAudio.audio.Stop ();
		Camera.main.audio.Stop ();
		Camera.main.audio.Play ();

		gameStates = GameStates.Run;

	}

	public void seeCredit()
	{
		gameStates = GameStates.Credits;

	}

	public void unpauseGame()
	{
		gameStates = GameStates.Run;
	}

	public void startMiniGame()
	{
		gameStates = GameStates.MiniGame;
	}

	public void winGame()
	{
		gameStates = GameStates.Win;
	}

	public void looseGame()
	{
		gameStates = GameStates.GameOver;
	}

	public void mainMenuDisplay()
	{
		gameStates = GameStates.Menu;
		Camera.main.audio.Stop ();
	}

	public void instructionsDisplay()
	{
		gameStates = GameStates.Instructions;

	}

}
