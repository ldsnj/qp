﻿using UnityEngine;
using System.Collections;

public class PlayerHealth : MonoBehaviour {

	public int maxHealth;
	public int hitOnWallMinusHealth;
	public GameObject gameManager;
	public GameObject healthLabel;

	private bool invincible;
	private int currentHealth;

	// Use this for initialization
	void Start () {
		invincible = false;
		currentHealth = maxHealth;
		healthLabel.gameObject.GetComponent<UILabel>().text = "Health : "+ currentHealth + "/"+ maxHealth;
	}
	
	// Update is called once per frame
	void Update () {
	
	}


	public void subHealth(int healthToRemove)
	{
		currentHealth -= healthToRemove;

		// Update the GUI for the Health
		healthLabel.gameObject.GetComponent<UILabel>().text = "Health : "+ currentHealth + "/"+ maxHealth;

		if(currentHealth == 0)
		{
			gameManager.GetComponent<GameManager>().looseGame();

		}



	}

	public void resetHealth()
	{

		currentHealth = maxHealth;
		healthLabel.gameObject.GetComponent<UILabel>().text = "Health : "+ currentHealth + "/"+ maxHealth;


	}

	public void removeInvincibility()
	{
		invincible = false;

	}

	void OnCollisionEnter2D(Collision2D other)
	{
		if(false == invincible)
		{
			if(other.collider.gameObject.tag.Equals ("Mur"))
			{

				subHealth(hitOnWallMinusHealth);
				invincible = true;
				Invoke ("removeInvincibility", 1.0f);
			}
		}

	}

}
