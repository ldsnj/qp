﻿using UnityEngine;
using System.Collections;

public class DevelHelp : MonoBehaviour {

	public GameObject lblFPSDisplay;
	private bool lblFPSActive;

	// Use this for initialization
	void Start () {
		lblFPSActive = false;
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown(KeyCode.F3))
		{
			if(lblFPSActive = false)
			{
				lblFPSDisplay.gameObject.SetActive(true);
				lblFPSActive = true;
			}
			else
			{
				lblFPSDisplay.gameObject.SetActive (false);
				lblFPSActive = false;
			}
			
		}

	}
}
