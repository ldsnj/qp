﻿using UnityEngine;
using System.Collections;

public class BallMovement : MonoBehaviour {

	float limit = 3.0f;
	public float journeyTime = 1.0f;

	Vector3 limit1;
	Vector3 limit2;

	public enum Direction{Horizontal, Vertical};
	public Direction movementAxis;

	public float speed = 1.0f;

	// Use this for initialization
	private float timer;
	void Start() {
		timer = 0.0f;

		if (movementAxis == Direction.Horizontal) {
			limit1 = new Vector3(limit+transform.position.x,transform.position.y,transform.position.z);
			limit2 = new Vector3(-limit+transform.position.x,transform.position.y,transform.position.z);
		} else {
			limit1 = new Vector3(transform.position.x,limit+transform.position.y,transform.position.z);
			limit2 = new Vector3(transform.position.x,-limit+transform.position.y,transform.position.z);
		}
	}
	
	// Update is called once per frame
	void Update() {
		timer += Time.deltaTime;
		transform.position=Vector3.Lerp(limit1, limit2, Mathf.PingPong(timer*speed, 1.0f));
	}
}
