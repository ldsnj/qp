﻿using UnityEngine;
using System.Collections;

public class MiniGameCenterCube : MonoBehaviour {

	public GameObject miniGameManager;

	void OnTriggerEnter(Collider other) {
		if (other.collider.gameObject.name == "BallH") {
			//Debug.Log ("BallH");
			miniGameManager.GetComponent<MiniGameManager>().setBallInCenter("BallH");
		}
		if (other.collider.gameObject.name == "BallV") {
			//Debug.Log ("BallV");
			miniGameManager.GetComponent<MiniGameManager>().setBallInCenter("BallV");
		}
	}

	void OnTriggerExit(Collider other) {
		if (other.collider.gameObject.name == "BallH") {
			//Debug.Log ("BallH");
			miniGameManager.GetComponent<MiniGameManager>().setBallOut("BallH");
		}
		if (other.collider.gameObject.name == "BallV") {
			//Debug.Log ("BallV");
			miniGameManager.GetComponent<MiniGameManager>().setBallOut("BallV");
		}
	}
}
