﻿using UnityEngine;
using System.Collections;

public class MiniGameManager : MonoBehaviour {


	public GameObject gameManager;
	bool isBallHInCenter = false;
	bool isBallVInCenter = false;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (isBallHInCenter && isBallVInCenter) {
			if(Input.GetKeyDown(KeyCode.Space))
  		    {
				GameManager gm = gameManager.GetComponent<GameManager>();
				gm.winGame();
			}
		}
	}

	public void setBallInCenter(string ball)
	{
		if (ball == "BallH")
			isBallHInCenter = true;
		if (ball == "BallV")
			isBallVInCenter = true;
	}

	public void setBallOut(string ball)
	{
		if (ball == "BallH")
			isBallHInCenter = false;
		if (ball == "BallV")
			isBallVInCenter = false;
	}
}
